# Minimal RPC example

## How to build & run

You can configure the amount of memory and iteration cound in client.rs.
Configure the IP to 10.0.5.1 for unikernels and 127.0.0.1 for native execution.

Build:
```sh
make
```

Run server
```
./server
```
run rust client
```
./client
```
run hermit rust client
```
sudo /usr/libexec/qemu-kvm -cpu qemu64,apic,fsgsbase,rdtscp,xsave,xsaveopt,fxsr,rdrand -display none -smp 1 -m 3G -serial stdio -netdev tap,id=net0,ifname=tap100,script=no,downscript=no,vhost=on -device virtio-net-pci,netdev=net0,disable-legacy=on -kernel rusty-loader-x86_64 -initrd ./hermit-bin
```

Expected output:
server:
```
eiling@ghost ~/p/c/minimal-rpc (master)> ./server
using prog=5055, vers=1
using TCP...
listening on port 53449
waiting for RPC requests...
rpc_memcpy_server_to_client_1_svc(offset=0, size=0x20000000)
rpc_memcpy_server_to_client_1_svc(offset=0, size=0x20000000)
rpc_memcpy_server_to_client_1_svc(offset=0, size=0x20000000)
rpc_memcpy_server_to_client_1_svc(offset=0, size=0x20000000)
rpc_memcpy_server_to_client_1_svc(offset=0, size=0x20000000)
rpc_memcpy_server_to_client_1_svc(offset=0, size=0x20000000)
rpc_memcpy_server_to_client_1_svc(offset=0, size=0x20000000)
rpc_memcpy_server_to_client_1_svc(offset=0, size=0x20000000)
rpc_memcpy_server_to_client_1_svc(offset=0, size=0x20000000)
rpc_memcpy_server_to_client_1_svc(offset=0, size=0x20000000)
rpc_memcpy_client_to_server_1_svc(offset=0, data len=0x20000000)
rpc_memcpy_client_to_server_1_svc(offset=0, data len=0x20000000)
rpc_memcpy_client_to_server_1_svc(offset=0, data len=0x20000000)
rpc_memcpy_client_to_server_1_svc(offset=0, data len=0x20000000)
rpc_memcpy_client_to_server_1_svc(offset=0, data len=0x20000000)
rpc_memcpy_client_to_server_1_svc(offset=0, data len=0x20000000)
rpc_memcpy_client_to_server_1_svc(offset=0, data len=0x20000000)
rpc_memcpy_client_to_server_1_svc(offset=0, data len=0x20000000)
rpc_memcpy_client_to_server_1_svc(offset=0, data len=0x20000000)
rpc_memcpy_client_to_server_1_svc(offset=0, data len=0x20000000)
```

rust client
```
eiling@ghost ~/p/c/minimal-rpc (master)> ./client
Hi
Bandwidth Server To Client: 1951.2 MiB/s
Bandwidth Client To Server: 1958.5 MiB/s
```

hermit rust client
```
...
[0][INFO] MTU: 1500 bytes
[0][WARN] Unable to read entropy! Fallback to a naive implementation!
Hi
Bandwidth Server To Client: 335.7 MiB/s
Bandwidth Client To Server: 90.5 MiB/s
[0][INFO] Number of interrupts
[0][INFO] [0][FPU]: 1
[0][INFO] [0][virtio_net]: 17
[0][INFO] [0][Timer]: 144
[0][INFO] Shutting down system
```
