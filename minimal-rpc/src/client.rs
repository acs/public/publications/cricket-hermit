use std::str::FromStr;
use std::time::Instant;
use std::vec::*;

extern crate rpc_lib;
use rpc_lib::include_rpcl;

use clap::Parser;

#[cfg(target_os = "hermit")]
use hermit_sys as _;

#[include_rpcl("src/rpc.x")]
struct RpcCon;

const MEMCPY_ITERATIONS: u32 = 10;
const MEM_SIZE: usize = 512 * 1024 * 1024;

#[derive(Parser)]
#[clap(name = "RPC Client", version = "0.1.0", author = "")]
struct Cli {
    #[clap(long, short)]
    memsize: Option<String>,
    
    #[clap(long, short)]
    server_ip: Option<String>,
}

fn memarg_parser(s: String) -> usize {
    if s.len() == 0 { 
        panic!("Invalid memsize argument");
    }   
    match s.chars().last().unwrap() {
        'K' => usize::from_str(&s[..s.len() - 1]).expect("") * 1024,
        'M' => usize::from_str(&s[..s.len() - 1]).expect("") * 1024 * 1024,
        'G' => usize::from_str(&s[..s.len() - 1]).expect("") * 1024 * 1024 * 1024,
        _ => usize::from_str(&s).expect(""),
    }   
}

fn main() {
    println!("Hi");
    let args = Cli::parse();

    let server_ip = match &args.server_ip {
        Some(ip) => ip,
        None => "10.0.5.1",
    };

    let memsize = match args.memsize {
        Some(memsize) => memarg_parser(memsize),
        None => MEM_SIZE,
    };

    let mut rpc_con =
        //RpcCon::new("127.0.0.1").expect("Failed to create Rpc-Connection to Server");
        RpcCon::new(server_ip).expect("Failed to create Rpc-Connection to Server");

    rpc_con
        .rpc_alloc(&(memsize as u64))
        .expect("failed to send rpc-request");

    let bandwidth = test_server_to_client_transfer(&mut rpc_con, memsize);
    println!("Bandwidth Server To Client: {0:.1} MiB/s", bandwidth);
    let bandwidth = test_client_to_server_transfer(&mut rpc_con, memsize);
    println!("Bandwidth Client To Server: {0:.1} MiB/s", bandwidth);
    
    rpc_con
        .rpc_check(&(memsize as u64))
        .expect("Failed to send RPC-Request");
    rpc_con
        .rpc_alloc(&0)
        .expect("failed to send rpc-request");
}

fn calculate_bandwidth(total_time_in_ms: u128, iterations: u32, memsize: usize) -> f64 {
    let time_per_copy_in_ms = total_time_in_ms as f64 / iterations as f64;
    let bandwidth_in_bytes_per_s = (memsize * 1024) as f64 / time_per_copy_in_ms;
    bandwidth_in_bytes_per_s / (1024 * 1024) as f64
}

fn test_server_to_client_transfer(rpc_con: &mut RpcCon, memsize: usize) -> f64 {
    // Iterations
    let time_begin = Instant::now();
    //let mut send_data = std::vec::Vec::new();
    //let mut receive_data = std::vec::Vec::with_capacity(memsize);
    for _i in 0..MEMCPY_ITERATIONS {
        let _mem = rpc_con
            .rpc_memcpy_server_to_client(&0, &(memsize as u64))
            .expect("Failed to send RPC-Request");
    }
    let elapsed_time = time_begin.elapsed().as_millis();

    // calculate bandwidth in MiB/s
    calculate_bandwidth(elapsed_time, MEMCPY_ITERATIONS, memsize)
}

fn test_client_to_server_transfer(rpc_con: &mut RpcCon, memsize: usize) -> f64 {
    // allocate host memory
    let mut client_mem: Vec<u8> = Vec::with_capacity(memsize);

    // initialize memory
    for i in 0..memsize {
        client_mem.push((i & 0xff) as u8);
    }

    // Iterations
    let time_begin = Instant::now();
    for _i in 0..MEMCPY_ITERATIONS {
        rpc_con
            .rpc_memcpy_client_to_server(&0, &client_mem)
            .expect("Failed to send RPC-Request");
    }
    let elapsed_time = time_begin.elapsed().as_millis();

    // calculate bandwidth in MiB/s
    calculate_bandwidth(elapsed_time, MEMCPY_ITERATIONS, memsize)
}
