
#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h> //unlink()
#include <signal.h> //sigaction
#include <sys/types.h>
#include <sys/stat.h>
#include <rpc/rpc.h>
#include "rpc.h"

#define CD_SOCKET_PATH "/tmp/rpc_sock"
enum socktype_t {UNIX, TCP, UDP} socktype;
enum socktype_t socktype = TCP;

static int connection_is_local = 0;
CLIENT *clnt;
static uint8_t* memory = NULL;
static size_t memory_size = 0;

extern void rpc_prog_1(struct svc_req *rqstp, register SVCXPRT *transp);

void int_handler(int signal) {
    if (socktype == UNIX) {
        unlink(CD_SOCKET_PATH);
    }
    printf("have a nice day!\n");
    svc_exit();
}

bool_t rpc_memcpy_client_to_server_1_svc(size_t offset, mem_data mem, int *result, struct svc_req *rqstp)
{
    printf("rpc_memcpy_client_to_server_1_svc(offset=%#0zx, data len=%#0zx)\n", offset, mem.mem_data_len);
    *result = 0;
    return 1;
}

bool_t rpc_memcpy_server_to_client_1_svc(size_t offset, size_t size, mem_result *result, struct svc_req *rqstp)
{
    printf("rpc_memcpy_server_to_client_1_svc(offset=%#0zx, size=%#0zx)\n", offset, size);
    if (offset + size > memory_size) {
        printf("offset + size > memory_size\n");
        result->err = 1;
        return 1;
    }
    result->mem_result_u.data.mem_data_val = memory;
    result->mem_result_u.data.mem_data_len = size;
    result->err = 0;
    return 1;
}

bool_t rpc_alloc_1_svc(size_t size, int *result, struct svc_req *rqstp)
{
    printf("rpc_alloc(size=%#0zx)\n", size);
    // realloc is malloc if memory is NULL and is free if size is 0
    if ((memory = realloc(memory, size)) == NULL && size != 0) {
        printf("realloc failed\n");
        *result = 1;
        return 1;
    }
    memory_size = size;

    for (size_t i = 0; i < size; i++) {
        memory[i] = (i & 0xFF);
    }
    *result = 0;
    return 1;
}

bool_t rpc_check_1_svc(size_t size, int *result, struct svc_req *rqstp)
{
    printf("rpc_check(size=%#0zx)\n", size);
    if (size > memory_size) {
        printf("offset + size > memory_size\n");
        *result = 1;
        return 1;
    }
    for (size_t i = 0; i < size; i++) {
        if (memory[i] != (i & 0xFF)) {
            printf("memory[%#0zx] != %#0zx\n", i, i & 0xFF);
            *result = 1;
            return 1;
        }
    }
    return 1;
}

void rpc_main(size_t prog_num, size_t vers_num)
{
    int ret = 1;
    register SVCXPRT *transp;

    int protocol = 0;
    int restore = 0;
    struct sigaction act;
    char *command = NULL;
    act.sa_handler = int_handler;
    sigaction(SIGINT, &act, NULL);

    printf("using prog=%d, vers=%d\n", prog_num, vers_num);


    switch (socktype) {
    case UNIX:
        printf("using UNIX...\n");
        transp = svcunix_create(RPC_ANYSOCK, 0, 0, CD_SOCKET_PATH);
        if (transp == NULL) {
            printf("cannot create service.\n");
            exit(1);
        }
        connection_is_local = 1;
        break;
    case TCP:
        printf("using TCP...\n");
        transp = svctcp_create(RPC_ANYSOCK, 0, 0);
        if (transp == NULL) {
            printf("cannot create service.\n");
            exit(1);
        }
        pmap_unset(prog_num, vers_num);
        printf("listening on port %d\n", transp->xp_port);
        protocol = IPPROTO_TCP;
        break;
    case UDP:
        /* From RPCGEN documentation:
         * Warning: since UDP-based RPC messages can only hold up to 8 Kbytes
         * of encoded data, this transport cannot be used for procedures that
         * take large arguments or return huge results.
         * -> Sounds like UDP does not make sense for CUDA, because we need to
         *    be able to copy large memory chunks
         **/
        printf("UDP is not supported...\n");
        break;
    }

    if (!svc_register(transp, prog_num, vers_num, rpc_prog_1, protocol)) {
        printf("unable to register (RPC_PROG_PROG, RPC_PROG_VERS).\n");
        exit(1);
    }

    printf("waiting for RPC requests...\n");

    svc_run();

    printf("svc_run returned. Cleaning up.\n");
    ret = 0;
 cleanup:
    pmap_unset(prog_num, vers_num);
    svc_destroy(transp);
    unlink(CD_SOCKET_PATH);
    printf("have a nice day!\n");
    exit(ret);
}

int rpc_prog_1_freeresult (SVCXPRT * a, xdrproc_t b , caddr_t c)
{
    if (b == (xdrproc_t) xdr_mem_result) {
        mem_result *res = (mem_result*)c;
        if (res->err == 0) {
            //free( (void*)res->mem_result_u.data.mem_data_val);
        }
    }
    return 1;
}


int main(int argc, char** argv)
{
    if (argc == 1) {
        rpc_main(RPC_PROG, RPC_VERS);
    } else if (argc == 2) {
        uint64_t vers;
        if (sscanf(argv[1], "%lu", &vers) != 1) {
            printf("version string could not be converted to number\n");
            printf("usage: %s [unique rpc version]\n", argv[0]);
            return 1;
        }
        rpc_main(RPC_PROG, vers);
    } else {
        printf("usage: %s [unique rpc version]\n", argv[0]);
    }
    return 0;
}