typedef opaque mem_data<>;
typedef unsigned hyper size_t;

union mem_result switch (int err) {
case 0:
    mem_data data;
default:
    void;
};

program RPC_PROG {
    version RPC_VERS {
        int          rpc_memcpy_client_to_server(size_t, mem_data) = 1;
        mem_result   rpc_memcpy_server_to_client(size_t, size_t)   = 2;
        int          rpc_alloc(size_t)                             = 3;
        int          rpc_check(size_t)                             = 4;
    } = 1;
} = 5055;
