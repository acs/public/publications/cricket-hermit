#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

rm -rf .unikraft/build/ .config* target

export YACC=/usr/bin/bison

export PATH=$PATH:$PWD

cargo +dev1 build --target x86_64-unikraft-linux-musl --release
