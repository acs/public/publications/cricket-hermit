#!/bin/bash

# Knobs
ITERATIONS=1
SERVER_VERSNUM=1
SERVER_ADDR="172.42.0.2"
RESULT_PATH="/global/research/cricketcore/cricket-hermit/evaluation/results"
TABLE_HEADER='printf "%-30s; %-10s; %-10s; %-10s\n" "Label" "DtoH" "HtoD" "Iterations"'

# Should not change
SERVER_PROGNUM=99
SERVER_GPU="2" # Tesla A100
SERVER_TASKSET="24-31,56-63"
CLIENT_TASKSET="24-27,56-59"
DATETIME_STR=$(date +"%Y-%m-%d_%H-%M")

# Paths
CRICKET_DIR="../cricket"
C_CLIENT="${CRICKET_DIR}/cpu/cricket-client.so"
HERMIT_LOADER="../rusty-loader-x86_64"
SERVER_PATH="${CRICKET_DIR}/cpu/cricket-rpc-server"

QEMU_BASE="sudo /usr/libexec/qemu-kvm \
    -enable-kvm \
    -cpu host,migratable=no,+invtsc,enforce \
    -smp 1 \
    -m 8G \
    -device isa-debug-exit,iobase=0xf4,iosize=0x04 \
    -display none -serial stdio"


CMDS=(  )
NAMES=(  )
AWKS=(  )

function start_rpc_server {
    rpcinfo -l $SERVER_ADDR $SERVER_PROGNUM $SERVER_VERSNUM | grep "tcp" > /dev/null
    RPC_AVAILABLE=$?

    if [ ! -f "${RESULT_PATH}" ]; then
        mkdir -p ${RESULT_PATH}
    fi

    SERVER_PID=0
    if [ $RPC_AVAILABLE -eq 0 ]; then
        echo "RPC service already running. Server output will not be collected."
    else
        echo "Starting RPC service"
        ssh -ftn ${SERVER_ADDR} "bash -c '\
                       CUDA_DEVICE_ORDER=PCI_BUS_ID \
                       CUDA_VISIBLE_DEVICES=${SERVER_GPU} \
                       nohup \
                       taskset -c ${SERVER_TASKSET} \
                       ${SERVER_PATH} ${SERVER_VERSNUM} > ${RESULT_PATH}/server_${DATETIME_STR}.log < /dev/null &'"

        if [ $? -ne 0 ]; then
            echo "Error starting RPC service"
            exit 1
        fi
    fi
}

function benchmark {
    echo "Executing $ITERATIONS times:";

    for i in ${!CMDS[@]}; do
        counter=0;
        CUR_NAME=${NAMES[$i]}
        CUR_FN=$(echo ${CUR_NAME} | tr ' ' '_')
        CUR_FILE=${RESULT_PATH}/${CUR_FN}_${DATETIME_STR}.log
        CUR_CMD=${CMDS[$i]}
        > ${CUR_FILE} 
        until [ $counter -eq $ITERATIONS ]; do
            echo "($counter/$ITERATIONS)";

            echo "running ${CUR_NAME} (${CUR_CMD})"
            taskset -c ${CLIENT_TASKSET} /usr/bin/time -f "TIME %U %S %e" bash -c "${CUR_CMD}" 2>&1 | tee -a ${CUR_FILE}

            if [ $? -ne 0 ]; then
                echo "proccess encountered error";
                exit 1;
            fi
            let counter+=1;
        done
    done
}

function print_results {
    for i in ${!CMDS[@]}; do
        CUR_NAME=${NAMES[$i]}
        CUR_FN=$(echo ${CUR_NAME} | tr ' ' '_')
        CUR_FILE=${RESULT_PATH}/${CUR_FN}_${DATETIME_STR}.log
        printf "%-30s; " "${CUR_NAME}"
        awk "${AWKS[$i]}" ${CUR_FILE}

    done
}

function run_bench {
    start_rpc_server
    sleep 1
    benchmark
}
