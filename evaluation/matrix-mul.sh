#!/bin/bash

DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi
source "$DIR/bench.sh"


# Knobs
ITERATIONS=10
SERVER_VERSNUM=1
SERVER_ADDR="172.42.0.2"
RESULT_PATH="/global/research/cricketcore/cricket-hermit/evaluation/results"

MM_C_CRICKET_NAME="MatrixMul C"
MM_C_CRICKET_BIN="${CRICKET_DIR}/tests/samples/samples/Samples/0_Introduction/matrixMul/matrixMul"
MM_C_CRICKET="CRICKET_RPCID=${SERVER_VERSNUM} \
                  REMOTE_GPU_ADDRESS=${SERVER_ADDR} \
                  LD_PRELOAD=${C_CLIENT} \
                  ${MM_C_CRICKET_BIN}"
MM_C_CRICKET_AWK='BEGIN{c=0;\
               t=0}\
               {\
                   if($1 == "TIME") {t += $4;c=c+1;}\
               }\
               END{\
                   printf "%-15.6f; %-10d\n", t/c, c;\
               }'
CMDS+=( "${MM_C_CRICKET}" )
NAMES+=( "${MM_C_CRICKET_NAME}" )
AWKS+=( "${MM_C_CRICKET_AWK}" )


MM_RUST_CRICKET_NAME="MatrixMul Rust"
MM_RUST_CRICKET="../cricket-rs/target/release/examples/matrixMul -c 172.42.0.2 --iterations 100000"
MM_RUST_CRICKET_AWK=$MM_C_CRICKET_AWK
CMDS+=( "${MM_RUST_CRICKET}" )
NAMES+=( "${MM_RUST_CRICKET_NAME}" )
AWKS+=( "${MM_RUST_CRICKET_AWK}" )

# Prepare VM with `ssh-copy-id root@<vm-ip> && scp bandwidth-test-rust <vm-ip>:~``
MM_RUST_VM_NAME="MatrixMul Rust VM"
MM_RUST_VM="ssh root@10.0.5.4 ./matrixMul -c 172.42.0.2 --iterations 100000"
MM_RUST_VM_AWK=$MM_RUST_CRICKET_AWK
# CMDS+=( "${MM_RUST_VM}" )
# NAMES+=( "${MM_RUST_VM_NAME}" )
# AWKS+=( "${MM_RUST_VM_AWK}" )


MM_HERMIT_NAME="MatrixMul Hermit"
MM_HERMIT_BIN="../cricket-rs/target/x86_64-unknown-hermit/release/examples/matrixMul"
MM_HERMIT="$QEMU_BASE \
                -netdev tap,id=net0,ifname=tap100,script=no,downscript=no,vhost=on \
                -device virtio-net-pci,netdev=net0,disable-legacy=on \
                -kernel ${HERMIT_LOADER} -initrd ${MM_HERMIT_BIN} \
                --append \"-- -c 172.42.0.2 --iterations 100000\""
MM_HERMIT_AWK=$MM_RUST_CRICKET_AWK
CMDS+=(  "${MM_HERMIT}" )
NAMES+=( "${MM_HERMIT_NAME}" )
AWKS+=(  "${MM_HERMIT_AWK}" )


MM_UNIKRAFT_NAME="MatrixMul Unikraft"
MM_UNIKRAFT_BIN="../cricket-rs/target/x86_64-unikraft-linux-musl/release/examples/matrixMul"
MM_UNIKRAFT="$QEMU_BASE \
                -netdev tap,id=net0,ifname=tap100,script=no,downscript=no,vhost=off \
                -device virtio-net-pci,netdev=net0,disable-legacy=off \
                -kernel ${MM_UNIKRAFT_BIN} \
                -append \"netdev.ipv4_addr=10.0.5.3 netdev.ipv4_gw_addr=10.0.5.1 netdev.ipv4_subnet_mask=255.255.255.0 -- -c 172.42.0.2 --iterations 100000\""
MM_UNIKRAFT_AWK=$MM_RUST_CRICKET_AWK
CMDS+=(  "${MM_UNIKRAFT}" )
NAMES+=( "${MM_UNIKRAFT_NAME}" )
AWKS+=(  "${MM_UNIKRAFT_AWK}" )

run_bench

echo "### RESULTS ###"
printf "%-30s; %-15s; %-10s\n" "Label" "Time" "Iterations"
print_results

echo "have a nice day!"
