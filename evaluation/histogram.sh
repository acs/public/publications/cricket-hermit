#!/bin/bash

DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi
source "$DIR/bench.sh"


# Knobs
ITERATIONS=10
SERVER_VERSNUM=1
SERVER_ADDR="172.42.0.2"
RESULT_PATH="/global/research/cricketcore/cricket-hermit/evaluation/results"

TABLE_HEADER='printf "%-30s; %-15s; %-15s; %-10s\n" "Label" "Time" "Kerneltime" "Iterations"'

HIST_C_CRICKET_NAME="Histogram C Cricket"
HIST_C_CRICKET_BIN="${CRICKET_DIR}/tests/samples/samples/Samples/2_Concepts_and_Techniques/histogram/histogram --sizemult=1"
HIST_C_CRICKET="CRICKET_RPCID=${SERVER_VERSNUM} \
                  REMOTE_GPU_ADDRESS=${SERVER_ADDR} \
                  LD_PRELOAD=${C_CLIENT} \
                  ${HIST_C_CRICKET_BIN}"
HIST_C_CRICKET_AWK='BEGIN{c=0;\
               kernels=0;\
               total=0}\
               {\
                   if($1 == "TIME") {t += $4;c=c+1;}\
               }\
               END{\
                   printf "%-15.6f; %-10d\n", t/c, c;\
               }'
CMDS+=( "${HIST_C_CRICKET}" )
NAMES+=( "${HIST_C_CRICKET_NAME}" )
AWKS+=( "${HIST_C_CRICKET_AWK}" )


HIST_RUST_CRICKET_NAME="Histogram Rust Cricket"
HIST_RUST_CRICKET="../cricket-rs/target/release/examples/histogram -c 172.42.0.2 --memsize=64Mi --iterations=10000"
HIST_RUST_CRICKET_AWK=$HIST_C_CRICKET_AWK
CMDS+=( "${HIST_RUST_CRICKET}" )
NAMES+=( "${HIST_RUST_CRICKET_NAME}" )
AWKS+=( "${HIST_RUST_CRICKET_AWK}" )

# Prepare VM with `ssh-copy-id root@<vm-ip> && scp bandwidth-test-rust <vm-ip>:~``
HIST_RUST_VM_NAME="Histogram Rust VM"
HIST_RUST_VM="ssh root@10.0.5.4 ./histogram -c 172.42.0.2 --memsize=64Mi --iterations=10000"
HIST_RUST_VM_AWK=$HIST_RUST_CRICKET_AWK
# CMDS+=( "${HIST_RUST_VM}" )
# NAMES+=( "${HIST_RUST_VM_NAME}" )
# AWKS+=( "${HIST_RUST_VM_AWK}" )

HIST_HERMIT_NAME="Histogram Hermit"
HIST_HERMIT_BIN="../cricket-rs/target/x86_64-unknown-hermit/release/examples/histogram"
HIST_HERMIT="$QEMU_BASE \
                -netdev tap,id=net0,ifname=tap100,script=no,downscript=no,vhost=on \
                -device virtio-net-pci,netdev=net0,disable-legacy=on \
                -kernel ${HERMIT_LOADER} -initrd ${HIST_HERMIT_BIN} -append \"--  -c 172.42.0.2 --memsize=64Mi --iterations=10000\""
HIST_HERMIT_AWK=$HIST_RUST_CRICKET_AWK
CMDS+=(  "${HIST_HERMIT}" )
NAMES+=( "${HIST_HERMIT_NAME}" )
AWKS+=(  "${HIST_HERMIT_AWK}" )


HIST_UNIKRAFT_NAME="Histogram Unikraft"
HIST_UNIKRAFT_BIN="../cricket-rs/target/x86_64-unikraft-linux-musl/release/examples/histogram"
HIST_UNIKRAFT="$QEMU_BASE \
                -netdev tap,id=net0,ifname=tap100,script=no,downscript=no,vhost=off \
                -device virtio-net-pci,netdev=net0,disable-legacy=off \
                -kernel ${HIST_UNIKRAFT_BIN} \
                -append \"netdev.ipv4_addr=10.0.5.3 netdev.ipv4_gw_addr=10.0.5.1 netdev.ipv4_subnet_mask=255.255.255.0 -- -c 172.42.0.2 --memsize=64Mi --iterations=10000\""
HIST_UNIKRAFT_AWK=$HIST_RUST_CRICKET_AWK
CMDS+=(  "${HIST_UNIKRAFT}" )
NAMES+=( "${HIST_UNIKRAFT_NAME}" )
AWKS+=(  "${HIST_UNIKRAFT_AWK}" )

run_bench

echo "### RESULTS ###"
printf "%-30s; %-15s; %-15s; %-10s\n" "Label" "Time" "Kernels" "Iterations"
print_results


echo "have a nice day!"
