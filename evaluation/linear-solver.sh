#!/bin/bash

DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi
source "$DIR/bench.sh"


# Knobs
ITERATIONS=10
SERVER_VERSNUM=1
SERVER_ADDR="172.42.0.2"
RESULT_PATH="/global/research/cricketcore/cricket-hermit/evaluation/results"

LS_MATRIX="${CRICKET_DIR}/tests/samples/samples/Samples/4_CUDA_Libraries/cuSolverDn_LinearSolver/gr_900_900_crg.mtx"

LS_C_CRICKET_NAME="LinearSolver C Cricket"
LS_C_CRICKET_BIN="${CRICKET_DIR}/tests/samples/samples/Samples/4_CUDA_Libraries/cuSolverDn_LinearSolver/cuSolverDn_LinearSolver"
LS_C_CRICKET="CRICKET_RPCID=${SERVER_VERSNUM} \
                  REMOTE_GPU_ADDRESS=${SERVER_ADDR} \
                  LD_PRELOAD=${C_CLIENT} \
                  ${LS_C_CRICKET_BIN} -R=lu -file ${LS_MATRIX}"
LS_C_CRICKET_AWK='BEGIN{c=0;\
               t=0}\
               {\
                   if($1 == "TIME") {t += $4;c=c+1;}\
               }\
               END{\
                   printf "%-15.6f; %-10d\n", t/c, c;\
               }'
CMDS+=( "${LS_C_CRICKET}" )
NAMES+=( "${LS_C_CRICKET_NAME}" )
AWKS+=( "${LS_C_CRICKET_AWK}" )


LS_RUST_CRICKET_NAME="LinearSolver Rust Cricket"
LS_RUST_CRICKET="../cricket-rs/target/release/examples/linearSolver -c 172.42.0.2"
LS_RUST_CRICKET_AWK=$LS_C_CRICKET_AWK
CMDS+=( "${LS_RUST_CRICKET}" )
NAMES+=( "${LS_RUST_CRICKET_NAME}" )
AWKS+=( "${LS_RUST_CRICKET_AWK}" )

# Prepare VM with `ssh-copy-id root@<vm-ip> && scp bandwidth-test-rust <vm-ip>:~``
LS_RUST_VM_NAME="LinearSolver Rust VM"
LS_RUST_VM="ssh root@10.0.5.4 ./linearSolver -c 172.42.0.2"
LS_RUST_VM_AWK=$LS_RUST_CRICKET_AWK
# CMDS+=( "${LS_RUST_VM}" )
# NAMES+=( "${LS_RUST_VM_NAME}" )
# AWKS+=( "${LS_RUST_VM_AWK}" )


LS_HERMIT_NAME="LinearSolver Hermit"
LS_HERMIT_BIN="../cricket-rs/target/x86_64-unknown-hermit/release/examples/linearSolver"
LS_HERMIT="$QEMU_BASE \
                -netdev tap,id=net0,ifname=tap100,script=no,downscript=no,vhost=on \
                -device virtio-net-pci,netdev=net0,disable-legacy=on \
                -kernel ${HERMIT_LOADER} -initrd ${LS_HERMIT_BIN} --append \"-- -c 172.42.0.2\""
LS_HERMIT_AWK=$LS_RUST_CRICKET_AWK
CMDS+=(  "${LS_HERMIT}" )
NAMES+=( "${LS_HERMIT_NAME}" )
AWKS+=(  "${LS_HERMIT_AWK}" )


LS_UNIKRAFT_NAME="LinearSolver Unikraft"
LS_UNIKRAFT_BIN="../cricket-rs/target/x86_64-unikraft-linux-musl/release/examples/linearSolver"
LS_UNIKRAFT="$QEMU_BASE \
                -netdev tap,id=net0,ifname=tap100,script=no,downscript=no,vhost=off \
                -device virtio-net-pci,netdev=net0,disable-legacy=off \
                -kernel ${LS_UNIKRAFT_BIN} \
                -append \"netdev.ipv4_addr=10.0.5.3 netdev.ipv4_gw_addr=10.0.5.1 netdev.ipv4_subnet_mask=255.255.255.0 -- -c 172.42.0.2\""
LS_UNIKRAFT_AWK=$LS_RUST_CRICKET_AWK
CMDS+=(  "${LS_UNIKRAFT}" )
NAMES+=( "${LS_UNIKRAFT_NAME}" )
AWKS+=(  "${LS_UNIKRAFT_AWK}" )

run_bench

echo "### RESULTS ###"
printf "%-30s; %-15s; %-10s\n" "Label" "Time" "Iterations"
print_results

echo "have a nice day!"
