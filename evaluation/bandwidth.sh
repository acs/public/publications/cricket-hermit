#!/bin/bash

DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi
source "$DIR/bench.sh"


# Knobs
ITERATIONS=10
SERVER_VERSNUM=1
SERVER_ADDR="172.42.0.2"
RESULT_PATH="/global/research/cricketcore/cricket-hermit/evaluation/results"


BW_TEST_C_CRICKET_NAME="BandwidthTest C Cricket"
BW_TEST_C_CRICKET_BIN="${CRICKET_DIR}/tests/samples/samples-bin/bandwidthTest.sample"
BW_TEST_C_CRICKET="CRICKET_RPCID=${SERVER_VERSNUM} \
                  REMOTE_GPU_ADDRESS=${SERVER_ADDR} \
                  LD_PRELOAD=/home/eiling/projects/cricket/cpu/cricket-client.so \
                  ${BW_TEST_C_CRICKET_BIN} --memory=pageable --csv --mode=range --start=536870912 --end=536870912 --increment=1"
BW_TEST_C_CRICKET_AWK='BEGIN{c=0;\
               h2d=0;\
               d2h=0}\
               {\
                   if($1 ~ /H2D/) {h2d += $4;c=c+1;}\
                   if($1 ~ /D2H/) d2h += $4;\
               }\
               END{\
                   printf "%-10.4f; %-10.4f; %-10d\n", d2h/c, h2d/c, c;\
               }'
CMDS+=( "${BW_TEST_C_CRICKET}" )
NAMES+=( "${BW_TEST_C_CRICKET_NAME}" )
AWKS+=( "${BW_TEST_C_CRICKET_AWK}" )


BW_TEST_RUST_CRICKET_NAME="BandwidthTest Rust Cricket"
BW_TEST_RUST_CRICKET="../cricket-rs/target/release/examples/bandwidth-test -c 172.42.0.2 --iterations 100"
BW_TEST_RUST_CRICKET_AWK='BEGIN{c=0;\
               h2d=0;\
               d2h=0}\
               {\
                   if($2 ~ /HTOD/) {h2d += $3;c=c+1;}\
                   if($2 ~ /DTOH/) d2h += $3;\
               }\
               END{\
                   printf "%-10.4f; %-10.4f; %-10d\n", d2h/(c*1024), h2d/(c*1024), c;\
               }'
CMDS+=( "${BW_TEST_RUST_CRICKET}" )
NAMES+=( "${BW_TEST_RUST_CRICKET_NAME}" )
AWKS+=( "${BW_TEST_RUST_CRICKET_AWK}" )

# Prepare VM with `ssh-copy-id root@<vm-ip> && scp bandwidth-test-rust <vm-ip>:~``
BW_TEST_RUST_VM_NAME="BandwidthTest Rust VM"
BW_TEST_RUST_VM="ssh root@10.0.5.4 ./bandwidth-test --cricket-server 172.42.0.2 --iterations 100"
BW_TEST_RUST_VM_AWK='BEGIN{c=0;\
               h2d=0;\
               d2h=0}\
               {\
                   if($2 ~ /HTOD/) {h2d += $3;c=c+1;}\
                   if($2 ~ /DTOH/) d2h += $3;\
               }\
               END{\
                   printf "%-10.4f; %-10.4f; %-10d\n", d2h/(c*1024), h2d/(c*1024), c;\
               }'
# CMDS+=( "${BW_TEST_RUST_VM}" )
# NAMES+=( "${BW_TEST_RUST_VM_NAME}" )
# AWKS+=( "${BW_TEST_RUST_VM_AWK}" )


BW_TEST_HERMIT_NAME="BandwidthTest Hermit"
BW_TEST_HERMIT_BIN="../cricket-rs/target/x86_64-unknown-hermit/release/examples/bandwidth-test"
#BW_TEST_HERMIT_BIN="../../old-stuff/cudasamples-bandwidth-test/target/x86_64-unknown-hermit/release/examples/bandwidth-test"
BW_TEST_HERMIT="${QEMU_BASE} \
                -netdev tap,id=net0,ifname=tap100,script=no,downscript=no,vhost=on \
                -device virtio-net-pci,netdev=net0,disable-legacy=on,host_mtu=9014 \
                -kernel ${HERMIT_LOADER} -initrd ${BW_TEST_HERMIT_BIN} -append \"-- --cricket-server 172.42.0.2 --iterations 10\""
# -netdev tap,id=net0,ifname=tap100,script=no,downscript=no,vhost=off -device virtio-net-pci,netdev=net0,disable-legacy=on,host_mtu=9014 -m 4G
BW_TEST_HERMIT_AWK=$BW_TEST_RUST_CRICKET_AWK
CMDS+=(  "${BW_TEST_HERMIT}" )
NAMES+=( "${BW_TEST_HERMIT_NAME}" )
AWKS+=(  "${BW_TEST_HERMIT_AWK}" )


BW_TEST_UNIKRAFT_NAME="BandwidthTest Unikraft"
BW_TEST_UNIKRAFT_BIN="../cricket-rs/target/x86_64-unikraft-linux-musl/release/examples/bandwidth-test"
BW_TEST_UNIKRAFT="$QEMU_BASE \
                -netdev tap,id=net0,ifname=tap100,script=no,downscript=no,vhost=off \
                -device virtio-net-pci,netdev=net0,disable-legacy=off \
                -kernel ${BW_TEST_UNIKRAFT_BIN} \
                -append \"netdev.ipv4_addr=10.0.5.3 netdev.ipv4_gw_addr=10.0.5.1 netdev.ipv4_subnet_mask=255.255.255.0 -- --cricket-server 172.42.0.2 --iterations 100\""
BW_TEST_UNIKRAFT_AWK=$BW_TEST_RUST_CRICKET_AWK
CMDS+=(  "${BW_TEST_UNIKRAFT}" )
NAMES+=( "${BW_TEST_UNIKRAFT_NAME}" )
AWKS+=(  "${BW_TEST_UNIKRAFT_AWK}" )

run_bench
echo "### RESULTS ###"
printf "%-30s; %-10s; %-10s; %-10s\n" "Label" "DtoH" "HtoD" "Iterations"
print_results

echo "have a nice day!"
