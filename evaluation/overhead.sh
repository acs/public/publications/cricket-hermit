#!/bin/bash

DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi
source "$DIR/bench.sh"


# Knobs
ITERATIONS=1
SERVER_VERSNUM=1
SERVER_ADDR="172.42.0.2"
RESULT_PATH="/global/research/cricketcore/cricket-hermit/evaluation/results"


OVERHEAD_C_CRICKET_NAME="Overhead C Cricket"
OVERHEAD_C_CRICKET_BIN="${CRICKET_DIR}/tests/test_apps/overhead.testapp"
OVERHEAD_C_CRICKET="CRICKET_RPCID=${SERVER_VERSNUM} \
                  REMOTE_GPU_ADDRESS=${SERVER_ADDR} \
                  LD_PRELOAD=${C_CLIENT} \
                  ${OVERHEAD_C_CRICKET_BIN}"
OVERHEAD_C_CRICKET_AWK='BEGIN{c=0;\
               c=0;}\
               {\
                   if($1 ~ /TOTALTIME/) {printf "%-20.6f; ", $2;}\
                   if($1 == "4.") {c = substr($6,2);}\
               }\
               END{\
                    printf "%-10d;\n", c;\
               }'
CMDS+=( "${OVERHEAD_C_CRICKET}" )
NAMES+=( "${OVERHEAD_C_CRICKET_NAME}" )
AWKS+=( "${OVERHEAD_C_CRICKET_AWK}" )


OVERHEAD_RUST_CRICKET_NAME="Overhead Rust Cricket"
OVERHEAD_RUST_CRICKET="../cricket-rs/target/release/examples/overhead --cricket-server 172.42.0.2 --iterations 100000"
OVERHEAD_RUST_CRICKET_AWK=$OVERHEAD_C_CRICKET_AWK
CMDS+=( "${OVERHEAD_RUST_CRICKET}" )
NAMES+=( "${OVERHEAD_RUST_CRICKET_NAME}" )
AWKS+=( "${OVERHEAD_RUST_CRICKET_AWK}" )

# Prepare VM with `ssh-copy-id root@<vm-ip> && scp bandwidth-test-rust <vm-ip>:~``
OVERHEAD_RUST_VM_NAME="Overhead Rust VM"
OVERHEAD_RUST_VM="ssh root@10.0.5.4 ./overhead --cricket-server 172.42.0.2 --iterations 100000"
OVERHEAD_RUST_VM_AWK=$OVERHEAD_C_CRICKET_AWK
# CMDS+=( "${OVERHEAD_RUST_VM}" )
# NAMES+=( "${OVERHEAD_RUST_VM_NAME}" )
# AWKS+=( "${OVERHEAD_RUST_VM_AWK}" )


OVERHEAD_HERMIT_NAME="Overhead Hermit"
OVERHEAD_HERMIT_BIN="../cricket-rs/target/x86_64-unknown-hermit/release/examples/overhead"
OVERHEAD_HERMIT="${QEMU_BASE} \
                -netdev tap,id=net0,ifname=tap100,script=no,downscript=no,vhost=on \
                -device virtio-net-pci,netdev=net0,disable-legacy=on \
                -kernel ${HERMIT_LOADER} -initrd ${OVERHEAD_HERMIT_BIN} -append \"-- --cricket-server 172.42.0.2 --iterations 100000\""
OVERHEAD_HERMIT_AWK=$OVERHEAD_RUST_CRICKET_AWK
CMDS+=(  "${OVERHEAD_HERMIT}" )
NAMES+=( "${OVERHEAD_HERMIT_NAME}" )
AWKS+=(  "${OVERHEAD_HERMIT_AWK}" )


OVERHEAD_UNIKRAFT_NAME="Overhead Unikraft"
OVERHEAD_UNIKRAFT_BIN="../cricket-rs/target/x86_64-unikraft-linux-musl/release/examples/overhead"
OVERHEAD_UNIKRAFT="$QEMU_BASE \
                -netdev tap,id=net0,ifname=tap100,script=no,downscript=no,vhost=off \
                -device virtio-net-pci,netdev=net0,disable-legacy=off \
                -kernel ${OVERHEAD_UNIKRAFT_BIN} \
                -append \"netdev.ipv4_addr=10.0.5.3 netdev.ipv4_gw_addr=10.0.5.1 netdev.ipv4_subnet_mask=255.255.255.0 -- --cricket-server 172.42.0.2 --iterations 100000\""
OVERHEAD_UNIKRAFT_AWK=$OVERHEAD_RUST_CRICKET_AWK
CMDS+=(  "${OVERHEAD_UNIKRAFT}" )
NAMES+=( "${OVERHEAD_UNIKRAFT_NAME}" )
AWKS+=(  "${OVERHEAD_UNIKRAFT_AWK}" )

run_bench
echo "### RESULTS ###"
printf "%-30s; %-20s; %-20s; %-20s; %-20s; %-10s;\n" "Label" "cudaGetDeviceCount" "cudaMalloc/cudaFree" "kernel launch w/o" "kernel launch w/" "count"
print_results

echo "have a nice day!"
