# Cricket Hermit Evaluation Repository

## Dependencies:
- CUDA 12.1 (if at other path than `/usr/loca/cuda` specifiy in `Makefile`)
- YACC (if at other path than `/usr/bin/bison` specify in `Makefile`)
- A GPU supporting sm_80 (if other sm is desired this has to be changed for all kernel compilations)
- Cargo and Rust

## Building
Get Submodules:
```
git submodule update --init --recursive
```

Download Hermit Loader and Kraftkit:
```
make rusty-loader-x86_64
make kraft
```

Build all binaries used in the evaluation:
```
make
```

Create Linux VM:
```
virt-builder fedora-37 \
    --format qcow2 \
    --output f37.qcow2 \
    --selinux-relabel \
    --root-password password:root \
    --update \
    --run-command 'dnf install -y libtirpc'
```

## Makefile targets
- rusty-loader-x86_64: Download Hermit Loader
- kraft: Download Unikraft kraftkit
- cricket: Build GPU Virtualization
- cricket-samples: C applications used in evaluation (Download from NVIDIA, apply patches, build)
- bandwidth-test: Build Rust version of bandwidthTest for Linux, Hermit and Unikraft
- linear-solver: Build Rust version of linearSolver for Linux, Hermit and Unikraft
- overhead: Build application for API latency measurements for Linux (C and Rust), Hermit and Unikraft
- matrix-mul: Build Rust version of matrixMul for Linux, Hermit and Unikraft
- histogram: Build Rust version of histogram for Linux, Hermit and Unikraft
- clean: clean up Cricket
- tap-device: setup Tap device that is used by the Linux VM, Hermit and Unikraft


## Running evaluation
There are scripts for each evaluation in the `evaluation` folder. Results are written to `stdout`. Outputs are additionally stored in `evaluation/results`.
If your network configuration is different change the variables at the top of the scripts to fit your setup.
All scripts should be executed from within the `evaluation` folder.

### BandwidthTest
```
./bandwidth.sh
```

### MatrixMul
```
./matrix-mul.sh
```

### linearSolver
```
./linear-solver.sh
```

### histogram
```
./histogram.sh
```

### overhead
```
./overhead.sh
```

### Linux VM
Start VM:
```
sudo nice -n 10 \
    taskset -c 21-28 \
    /usr/libexec/qemu-kvm \
    -cpu host,migratable=no,+invtsc \
    -hda <path-to-image>/f37.qcow2 \
    -m 8G \
    -smp 1 \
    -nographic \
    -nic tap,ifname=tap100,vhost=on,vhostforce=on,script=no,downscript=no,model=virtio-net-pci
```
Correctly setup networking in VM (correct IP, MTU 9000) and copy all application binaries into VM.
The commands to execute tests for the VM are also located in the scripts in the `evaluation` folder but are commented out per default.
