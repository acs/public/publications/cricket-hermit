SMS = 80
CUDA_PATH = /usr/local/cuda
export YACC := /usr/bin/bison
export PATH := $(PWD):$(PATH)
export HERMIT_MTU := 9000

.PHONY: all clean cricket bandwidth-test linear-solver cricket-samples overhead

all: cricket cricket-samples bandwidth-test linear-solver cricket-samples matrix-mul histogram overhead

rusty-loader-x86_64:
	curl -LO "https://github.com/hermitcore/rusty-loader/releases/download/v0.4.4/rusty-loader-x86_64"

kraft:
	curl -L "https://github.com/unikraft/kraftkit/releases/download/v0.6.4/kraftkit_0.6.4_linux_amd64.tar.gz" | tar xvz kraft

cricket:
	patch -N -i cricket.patch \
		cricket/cpu/cpu-client-runtime.c -s || true
	$(MAKE) -C cricket libtirpc
	$(MAKE) -C cricket/cpu LOG=INFO
	$(MAKE) -C cricket/tests/samples samples

cricket-samples: cricket
	make -C cricket/tests/samples/samples/Samples/0_Introduction/matrixMul \
		clean
	patch -N -i matrixMul.patch \
		cricket/tests/samples/samples/Samples/0_Introduction/matrixMul/matrixMul.cu -s || true
	make -C cricket/tests/samples/samples/Samples/0_Introduction/matrixMul \
		NVCCFLAGS="-cudart shared -O3" \
		SMS="${SMS}" \
		CPATH="samples/Common" \
		CUDA_PATH=${CUDA_PATH}
	
	make -C cricket/tests/samples/samples/Samples/1_Utilities/bandwidthTest \
		clean
	make -C cricket/tests/samples/samples/Samples/1_Utilities/bandwidthTest \
		NVCCFLAGS="-cudart shared -O3" \
		SMS="${SMS}" \
		CPATH="samples/Common" \
		CUDA_PATH=${CUDA_PATH}

	
	make -C cricket/tests/samples/samples/Samples/4_CUDA_Libraries/cuSolverDn_LinearSolver \
		clean
	patch -N -i linearSolver_add_loop.patch \
		cricket/tests/samples/samples/Samples/4_CUDA_Libraries/cuSolverDn_LinearSolver/cuSolverDn_LinearSolver.cpp -s || true
	make -C cricket/tests/samples/samples/Samples/4_CUDA_Libraries/cuSolverDn_LinearSolver \
		NVCCFLAGS="-cudart shared -O3" \
		SMS="${SMS}" \
		CPATH="samples/Common" \
		CUDA_PATH=${CUDA_PATH}

	make -C cricket/tests/samples/samples/Samples/2_Concepts_and_Techniques/histogram \
		clean
	make -C cricket/tests/samples/samples/Samples/2_Concepts_and_Techniques/histogram \
		NVCCFLAGS="-cudart shared -O3" \
		SMS="${SMS}" \
		CPATH="samples/Common" \
		CUDA_PATH=${CUDA_PATH}

bandwidth-test:
	cargo build --manifest-path cricket-rs/Cargo.toml --example bandwidth-test --release
	cargo build --manifest-path cricket-rs/Cargo.toml --example bandwidth-test --release -Zbuild-std=std,panic_abort --target x86_64-unknown-hermit
	cargo build --manifest-path cricket-rs/Cargo.toml --example bandwidth-test --release -Zbuild-std=std,panic_abort --target x86_64-unikraft-linux-musl
	
linear-solver:
	cargo build --manifest-path cricket-rs/Cargo.toml --example linearSolver --release
	cargo build --manifest-path cricket-rs/Cargo.toml --example linearSolver --release -Zbuild-std=std,panic_abort --target x86_64-unknown-hermit
	cargo build --manifest-path cricket-rs/Cargo.toml --example linearSolver --release -Zbuild-std=std,panic_abort --target x86_64-unikraft-linux-musl

overhead:
	make -C cricket/tests/test_apps overhead ARCH=sm_80
	cargo build --manifest-path cricket-rs/Cargo.toml --example overhead --release
	cargo build --manifest-path cricket-rs/Cargo.toml --example overhead --release -Zbuild-std=std,panic_abort --target x86_64-unknown-hermit
	cargo build --manifest-path cricket-rs/Cargo.toml --example overhead --release -Zbuild-std=std,panic_abort --target x86_64-unikraft-linux-musl
	
matrix-mul:
	cargo build --manifest-path cricket-rs/Cargo.toml --example matrixMul --release
	cargo build --manifest-path cricket-rs/Cargo.toml --example matrixMul --release -Zbuild-std=std,panic_abort --target x86_64-unknown-hermit
	cargo build --manifest-path cricket-rs/Cargo.toml --example matrixMul --release -Zbuild-std=std,panic_abort --target x86_64-unikraft-linux-musl
	

histogram:
	cargo build --manifest-path cricket-rs/Cargo.toml --example histogram --release
	cargo build --manifest-path cricket-rs/Cargo.toml --example histogram --release -Zbuild-std=std,panic_abort --target x86_64-unknown-hermit
	cargo build --manifest-path cricket-rs/Cargo.toml --example histogram --release -Zbuild-std=std,panic_abort --target x86_64-unikraft-linux-musl

clean:
	$(MAKE) -C cricket clean
	
tap-device:
	ip tuntap add tap100 mode tap
	ip addr add 10.0.5.1/24 broadcast 10.0.5.255 dev tap100
	ip link set dev tap100 up
	echo 1 > /proc/sys/net/ipv4/conf/tap100/proxy_arp
	ifconfig tap100 mtu 9000